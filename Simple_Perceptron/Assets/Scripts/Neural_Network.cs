﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Neural_Network : MonoBehaviour
{
    //static void Main(string[] args)
    public void Main()
    {
        int[,] inputValue = { { 0, 0 }, { 1, 0 }, { 0, 1 }, { 1, 1 } };//должно быть в инспекторе
        int[] outputValue = { 0, 1, 1, 1 };//должно быть в инспекторе

        System.Random rnd = new System.Random();//*1
        double[] weight = { GetRandomDouble(-1, 1), GetRandomDouble(-1, 1) };
        double bias = GetRandomDouble(-1, 1);

        double error = 1;
        int loops = 0;

        while (error > 0.5)
        {
            error = 0;
            for (int i = 0; i < 4; i++)
            {
                int output = CalculateOutput(inputValue[i, 0], inputValue[i, 1], bias, weight);
                int localError = outputValue[i] - output;
                Debug.Log($"Pair ({inputValue[i, 0]}, {inputValue[i, 1]}), output: {output}");
                //Console.WriteLine($"Pair ({inputValue[i, 0]}, {inputValue[i, 1]}), output: {output}");

                weight[0] = inputValue[i, 0] * localError + weight[0];
                weight[1] = inputValue[i, 1] * localError + weight[1];
                bias += localError;
                error += Mathf.Abs(localError);
                Debug.Log("_________________________________________________________________________________");
            }
            loops++;
            Debug.Log("==================================================================================");
        }
        Debug.Log(loops);
    }

    private static int CalculateOutput(double input1, double input2, double bias, double[] weight)
    {
        double result = input1 * weight[0] + input2 * weight[1] + bias;
        Debug.Log($"Result = {input1 * weight[0]} * {input2 * weight[1]} + {bias}");
        //Console.WriteLine($"Result = {input1 * weight[0]} * {input2 * weight[1]} + {bias}");
        return (result >= 0) ? 1 : 0;
    }

    private static double GetRandomDouble(double min, double max)
    {
        System.Random rnd = new System.Random(); //*1
        return rnd.NextDouble() * (max - min) + min;
    }
}
//*1 System.Random VS UnityEngine.Random